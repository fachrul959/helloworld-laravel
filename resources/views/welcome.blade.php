<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        body {
            display: flex;
            height: 100vh;
        }
        h1 {
            margin: auto;
            font-size: 5rem;
            background-color: #2C3333;
            color: #F5F2E7;
            border-radius: 25px;
            padding: 2rem 4rem;
        }
    </style>
    <title>Tugas 2 | Setup Hello World</title>
</head>
<body>
    <h1>Hello World</h1>
</body>
</html>